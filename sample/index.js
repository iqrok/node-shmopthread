const {
		ShmopThread,
		Constant,
		Datatype,
	} = require('..');

const {
		O_FLAG,
		DIRECTION,
		PROTECTION
	} = Constant;

const pShm = new ShmopThread();
pShm.config({
			fname: 'shm-out.tmp',
			flag: O_FLAG.CREAT | O_FLAG.RDWR,
			mode: 0o777,
			direction: DIRECTION.READ,
			protection: PROTECTION.READ,
			period: 25000, // in us
			alignByte: true,
			persistence: false,
		})
	.setData([
			pShm.construct('uint8', Datatype.UInt8),
			pShm.construct('int16', Datatype.Int16),
			pShm.construct('double', Datatype.Float64),
			pShm.construct('int32', Datatype.Int32),
			pShm.construct('float', Datatype.Float32),
		])
	.init();

setInterval(() => {
	const buffer = pShm.read();
	const values = pShm.readStruct(buffer);
	console.log(values);
}, 25)

const pShm2 = new ShmopThread();
pShm2.setFileName('shm-in.tmp');
pShm2.setFlag(O_FLAG.CREAT | O_FLAG.RDWR);
pShm2.setMode(0o777);
pShm2.setDirection(DIRECTION.WRITE);
pShm2.setProtection(PROTECTION.WRITE | PROTECTION.READ);
pShm2.setPeriod(25000);
pShm2.setData([
			pShm.construct('uint8', Datatype.UInt8),
			pShm.construct('int16', Datatype.Int16),
			pShm.construct('double', Datatype.Float64),
			pShm.construct('int32', Datatype.Int32),
			pShm.construct('float', Datatype.Float32),
		]);
pShm2.init();

setInterval(() => {
	const values = pShm.values;
	pShm2.writeStruct(values);
}, 25);

setTimeout(() => {
		pShm.release();
		pShm2.release();
		process.exit(0);
	}, 10000);
