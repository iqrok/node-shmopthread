extern "C" {

#include <unistd.h>
#include <sys/time.h>
#include <time.h>

}

#include <cstdint>
#include <cstring>
#include <chrono>
#include <thread>

#include <napi.h>
#include <ShmopThread.h>

#define NSEC_PER_SEC 1E9

class ShmopThreadJS : public Napi::ObjectWrap<ShmopThreadJS> {
	public:
		static Napi::Object Init(Napi::Env env, Napi::Object exports);
		ShmopThreadJS(const Napi::CallbackInfo& info);

	private:
		ShmopThread pShm;
		void init(const Napi::CallbackInfo& info);
		void release(const Napi::CallbackInfo& info);
		void set_file_name(const Napi::CallbackInfo& info);
		void set_flag(const Napi::CallbackInfo& info);
		void set_mode(const Napi::CallbackInfo& info);
		void set_direction(const Napi::CallbackInfo& info);
		void set_byte_size(const Napi::CallbackInfo& info);
		Napi::Value get_byte_size(const Napi::CallbackInfo& info);
		void set_prot(const Napi::CallbackInfo& info);
		void set_period_us(const Napi::CallbackInfo& info);
		void set_preserve_link(const Napi::CallbackInfo& info);

		Napi::Value read(const Napi::CallbackInfo& info);
		void write(const Napi::CallbackInfo& info);

		uint8_t *buffer;
		uint32_t byte_size;
		std::string fname;
};

Napi::Object ShmopThreadJS::Init(Napi::Env env, Napi::Object exports) {
    // This method is used to hook the accessor and method callbacks
    Napi::Function func = DefineClass(env, "shmopthread", {
        InstanceMethod<&ShmopThreadJS::init>("init", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::release>("release", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_file_name>("setFileName", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_flag>("setFlag", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_mode>("setMode", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_direction>("setDirection", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_byte_size>("setByteSize", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::get_byte_size>("getByteSize", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_prot>("setProtection", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_period_us>("setPeriod", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::set_preserve_link>("setPersistence", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::read>("read", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
        InstanceMethod<&ShmopThreadJS::write>("write", static_cast<napi_property_attributes>(napi_writable | napi_configurable)),
    });

    Napi::FunctionReference* constructor = new Napi::FunctionReference();

    *constructor = Napi::Persistent(func);
    exports.Set("ShmopThreadJS", func);

    env.SetInstanceData<Napi::FunctionReference>(constructor);

    return exports;
}

ShmopThreadJS::ShmopThreadJS(const Napi::CallbackInfo& info)
									: Napi::ObjectWrap<ShmopThreadJS>(info) {
}

void ShmopThreadJS::set_file_name(const Napi::CallbackInfo& info){
	this->fname = info[0].As<Napi::String>();
    this->pShm.set_file_name(this->fname);
}

void ShmopThreadJS::set_flag(const Napi::CallbackInfo& info){
    this->pShm.set_flag(info[0].As<Napi::Number>().Int32Value());
}

void ShmopThreadJS::set_mode(const Napi::CallbackInfo& info){
    this->pShm.set_mode(info[0].As<Napi::Number>().Int32Value());
}

void ShmopThreadJS::set_direction(const Napi::CallbackInfo& info){
    this->pShm.set_direction(info[0].As<Napi::Number>().Uint32Value());
}

void ShmopThreadJS::set_byte_size(const Napi::CallbackInfo& info){
	this->byte_size = info[0].As<Napi::Number>().Uint32Value();
    this->pShm.set_byte_size(byte_size);
    this->buffer = new uint8_t[byte_size];
    this->pShm.attach_data(this->buffer);
}

Napi::Value ShmopThreadJS::get_byte_size(const Napi::CallbackInfo& info){
	Napi::Env env = info.Env();
	return Napi::Number::New(env, byte_size);
}

void ShmopThreadJS::set_prot(const Napi::CallbackInfo& info){
    this->pShm.set_prot(info[0].As<Napi::Number>().Int32Value());
}

void ShmopThreadJS::set_period_us(const Napi::CallbackInfo& info){
    this->pShm.set_period_us(info[0].As<Napi::Number>().Uint32Value());
}

void ShmopThreadJS::set_preserve_link(const Napi::CallbackInfo& info){
    this->pShm.set_preserve_link(info[0].As<Napi::Boolean>().Value());
}

void ShmopThreadJS::init(const Napi::CallbackInfo& info){
    this->pShm.init();
}

void ShmopThreadJS::release(const Napi::CallbackInfo& info){
    this->pShm.uninit();
}

Napi::Value ShmopThreadJS::read(const Napi::CallbackInfo& info){
	Napi::Env env = info.Env();

    this->pShm.read(this->buffer);
    return Napi::Buffer<uint8_t>::New(env, this->buffer, this->byte_size);
}

void ShmopThreadJS::write(const Napi::CallbackInfo& info){
	if(!this->pShm.isInitialized) return;

	Napi::Buffer<uint8_t> req = info[0].As<Napi::Buffer<uint8_t>>();

	// if buffer length > byte_size,
	// then only first byte_size data will be copied
	this->buffer = req.Data();
	this->pShm.write(this->buffer);
}

// Initialize native add-on
Napi::Object Init(Napi::Env env, Napi::Object exports) {
	ShmopThreadJS::Init(env, exports);
	return exports;
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, Init);
