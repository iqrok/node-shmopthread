function _padBuffer(buffer, length, padding){
	if(padding <= 0) return buffer;

	return Buffer.concat(
			[ Buffer.alloc(padding, 0), buffer ],
			length + padding,
		);
}

const UInt8 = {
	length: 1,
	val(buffer, offset = 0){
		return buffer.readUInt8(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Uint8Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const Int8 = {
	length: 1,
	val(buffer, offset = 0){
		return buffer.readInt8(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Int8Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const UInt16 = {
	length: 2,
	val(buffer, offset = 0){
		return buffer.readUInt16LE(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Uint16Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const Int16 = {
	length: 2,
	val(buffer, offset = 0){
		return buffer.readInt16LE(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Int16Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const UInt32 = {
	length: 4,
	val(buffer, offset = 0){
		return buffer.readUInt32LE(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Uint32Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const Int32 = {
	length: 4,
	val(buffer, offset = 0){
		return buffer.readInt32LE(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Int32Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const Float32 = {
	length: 4,
	val(buffer, offset = 0){
		return buffer.readFloatLE(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Float32Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const Float64 = {
	length: 8,
	val(buffer, offset = 0){
		return buffer.readDoubleLE(offset);
	},
	bytes(num, padding = 0){
		const buffer = new ArrayBuffer(this.length);
		const view = new Float64Array(buffer);

		view[0] = num;

		return _padBuffer(
				Buffer.from(new Uint8Array(buffer)),
				this.length,
				padding
			);
	},
};

const Float = Float32;

const Double = Float64;

module.exports = {
		UInt8,
		UInt16,
		UInt32,
		Int8,
		Int16,
		Int32,
		Float32,
		Float64,
		Float,
		Double,
	};
