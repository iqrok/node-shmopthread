const EventEmitter = require('events');
const { ShmopThreadJS } = require('bindings')('shmopthread');
const Constant = require(`${__dirname}/constants.lib.js`);
const Datatype = require(`${__dirname}/datatype.lib.js`);

const { O_FLAG, DIRECTION, PROTECTION } = Constant;

class ShmopThread extends ShmopThreadJS {
	constructor(opts = null){
		super();

		const self = this;

		self._isLittleEndian = true;
		self._isStructPadded = false;

		// default config
		self._config = {
			fname: 'shm-out.tmp',
			flag: O_FLAG.CREAT | O_FLAG.CREAT,
			mode: 0o777,
			direction: DIRECTION.READ,
			protection: PROTECTION.READ,
			period: 50, // in us
			length: 8,
			alignByte: true,
			persistence: true,
		};

		self._data = [];
		self.values = [];
	}

	config(options = {}){
		const self = this;

		for(const key in options){
			if(self._config[key] === undefined) continue;
			self._config[key] = options[key];
		}

		self.setFileName(self._config.fname);
		self.setFlag(self._config.flag);
		self.setMode(self._config.mode);
		self.setDirection(self._config.direction);
		self.setProtection(self._config.protection);
		self.setPeriod(self._config.period);
		self.setByteSize(self._config.length);
		self.setPersistence(self._config.persistence);

		return self;
	}

	construct(id, datatype, value){
		if(id == undefined || datatype == undefined){
			throw new Error('id and datatype must be defined!!');
		}

		return {
				id,
				datatype,
				offset: 0,
				padding: 0,
				value: 0,
			};
	}

	setData(arrOfData = []){
		const self = this;

		for(const item of arrOfData){
			self._data.push(item);
		}

		self._alignBytes();

		return self;
	}

	_findAlignment(length){
		if(length > 4) return 8;
		if(length > 2) return 4;
		if(length > 1) return 2;
		return 1;
	}

	_alignBytes(){
		const self = this;

		const length = self._data.length;
		const address = {
				start: 0,
				end: 0,
			};

		for(let idx = 0; idx < length; idx++){
			const item = self._data[idx];
			const actualSize = item.datatype.length;
			const alignTo = self._findAlignment(actualSize);

			address.end = actualSize + address.start;
			const isAligned = address.end % alignTo;

			if(
				alignTo <= 1 // char is not padded in struct
				|| isAligned === 0 // already aligned
				|| !self._config.alignByte // skip padding if disabled
			) {
				item.offset = address.start;
				item.padding = 0;
				item.address = { ...address };
				self.values[item.id] = item;

				address.start = address.end;
				continue;
			}

			// padded address, get how many byte is padding
			const alignDiff = alignTo - isAligned;

			// got to the actual aligned byte
			address.end += alignDiff;

			// set the actual address based on the calculated padding
			item.padding = alignDiff;
			item.offset = address.start + (self._isLittleEndian ? item.padding : 0);
			item.address = { ...address };
			self.values[item.id] = item;

			// set next start address from current end address
			address.start = address.end;
		}

		self.setByteSize(address.end);
	}

	readStruct(buffer){
		const self = this;

		for(const item of self._data){
			const sliced = buffer.slice(
					item.offset,
					item.offset + item.datatype.length
				);

			item.value = item.datatype.val(sliced);

			self.values[item.id] = item;
		}

		return self.values;
	}

	writeStruct(data){
		const self = this;

		const bytes = [];

		for(const key in data){
			const item = data[key];
			item.buffer = item.datatype.bytes(item.value, item.padding);
			bytes.push(item.buffer);
			self.values[item.id] = item;
		}

		const buffer = Buffer.concat(bytes);
		self.write(buffer);

		return self.values;
	}

	start(){
		const self = this;
		return self;
	}
}

module.exports = {
	ShmopThread,
	Constant,
	Datatype,
};
