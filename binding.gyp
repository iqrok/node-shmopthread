{
	"targets": [{
		"target_name": "shmopthread",
		"include_dirs": [
			"<!@(node -p \"require('node-addon-api').include\")",
			"./src/include"
		],
		"sources": [
			"./src/main.cpp",
			"./src/ShmopThread.cpp"
		],
		"link_settings": {
			"libraries": [
			],
			"library_dirs": [
				"/usr/local/lib/",
				"<(module_root_dir)/src/lib/"
			]
		},
		"cflags": [
			"-std=c++17",
			"-fexceptions",
			"-lstdc++",
			"-Iinclude",
			"-O2",
			"-lrt",
			"-lpthread"
		],
		"cflags_cc": [
			"-std=c++17",
			"-fexceptions",
			"-lstdc++",
			"-Iinclude",
			"-O2",
			"-lrt",
			"-lpthread"
		],
		"defines": [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ]
	}]
}
